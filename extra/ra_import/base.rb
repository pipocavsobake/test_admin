# frozen_string_literal: true

module RaImport
  # Base class for all xls and xlsx imports; should be wrapped by rescue
  class Base
    attr_accessor :err
    def initialize(file)
      @file = file
      @error = nil
    end

    def parse_xlsx(xls)
      xls.sheets.each do |s|
        xls.default_sheet = s
        next if xls.last_row.nil?
        parse_sheet(xls)
      end
    end

    def parse_xls(xls)
      xls.each_with_pagename do |_name, s|
        next if s.nil?
        parse_sheet(s)
      end
    end

    def parse
      xls = Roo::Spreadsheet.open(@file.to_s)
      before_process
      return parse_xlsx(xls) if xls.class.name == 'Roo::Excelx'
      parse_xls(xls)
    end

    def save_or_raise(obj, hash, idx)
      return if obj.save
      self.err = "Ошибка валидации: #{obj.errors.full_messages.join(', ')}" \
                 "в данных #{hash.inspect} на строке #{idx}"
      raise ActiveRecord::Rollback
    end

    def row_invalid?(hash)
      return true if hash.class.name == 'Fixnum'
      false
    end

    def append_err(error, hash, idx)
      self.err = err || ''
      self.err += "Ошибка: #{error.class.name}: #{error.message}\n"\
        "#{error.backtrace[0..5].join("\n")} "\
        "Парсим #{hash.inspect} строка #{idx}"
      raise ActiveRecord::Rollback
    end

    def parse_sheet(sheet)
      self.err = nil
      ActiveRecord::Base.transaction do
        sheet.each_with_index do |hash, i|
          next if row_invalid?(hash)
          process_row(hash, i)
        rescue StandardError, SyntaxError => e
          append_err(e, hash, i)
        end
      end
      raise StandardError, err unless err.nil?
    end

    def self.prepare_attachment(name, uploaded_io)
      return nil if uploaded_io.nil?
      ts = Time.now.strftime('%Y-%m-%d_%H%M%S')
      ext = File.extname(uploaded_io.original_filename)
      fn = "go_import_#{name}_#{ts}#{ext}"
      fpath = Rails.root.join('tmp', fn)
      File.open(fpath, 'wb') { |file| file.write(uploaded_io.read) }
      fpath
    end
  end
end
