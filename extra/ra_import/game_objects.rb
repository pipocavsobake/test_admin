# frozen_string_literal: true

module RaImport
  # import game_object tables
  class GameObjects < Base
    attr_accessor :games, :pair_games, :keys, :game, :archive, :types

    def row_invalid?(hash)
      return true if hash.class.name == 'Fixnum'
      false
    end

    def before_process
      @games ||= []
      @pair_games ||= []
      @games.each { |game| game.game_objects = [] }
      init_types
      extract_archive
      @pair_games.each { |game| game.game_objects = [] }
      true
    end

    def init_types
      @game = @games.first
      @types = game&.question_field_data&.all&.to_a
      return unless types
      @types = types.each_with_object({}) do |qfd, o|
        o[qfd.name] = qfd.kind
      end
    end

    def dir
      Rails.root.join('public', path).to_s
    end

    def path
      "tests/#{game.id}/"
    end

    def extract_entry(entry)
      return if File.extname(entry.name).blank?
      to = "#{gen_key}#{File.extname(entry.name)}"
      @paths[entry.name] = "/#{path}#{to}"
      entry.extract("#{dir}#{to}")
    end

    def extract_archive
      return unless archive
      @paths = {}
      FileUtils.mkdir_p(dir)
      paths = Dir.glob("#{dir}*")
      FileUtils.rm(paths) unless path.blank?
      Zip::File.open(archive) do |zip_file|
        zip_file.each(&method(:extract_entry))
      end
    end

    def gen_key
      key = SecureRandom.hex(10)
      return gen_key if @paths.values.include? key
      key
    end

    def process_first_row(hash)
      @keys = hash
      @games.each { |game| game.check_keys!(@keys) }
      @pair_games.each { |game| game.check_keys!(@keys) }
    end

    def process_row(hash, idx)
      return process_first_row(hash) if idx.zero?
      obj = @keys.each_with_object(Hash.new('')).with_index do |(key, o), i|
        kind = types && types[key]
        o[key] = kind == 'image' ? @paths[hash[i]] : hash[i]
      end
      GameObject.create(data: obj, games: @games, pair_games: @pair_games)
    end
  end
end
