# frozen_string_literal: true

module RailsAdmin
  module Config
    module Actions
      # imports xls or xlsx file and saves GameObjects
      class GoImport < RailsAdmin::Config::Actions::Base
        RailsAdmin::Config::Actions.register(self)

        register_instance_option :member? do
          true
        end

        register_instance_option :http_methods do
          %i[get post]
        end

        register_instance_option :controller do
          proc do
            next if request.get?
            @o = @object = @abstract_model.model.unscoped.find(params[:id])
            klass = @o.class.name.underscore
            fpath = RaImport::Base.prepare_attachment(klass, params[:file])
            begin
              parser = RaImport::GameObjects.new(fpath)
              parser.archive = RaImport::Base.prepare_attachment(klass, params[:archive])
              parser.send("#{klass.pluralize}=", [@o])
              parser.parse
              msg = { success: 'Успешно' }
              redirect_back(fallback_location: index_path(klass), flash: msg)
            rescue StandardError, ScriptError => e
              Raven.capture_exception(e)
              Rails.logger.fatal "#{e.message}\n#{e.backtrace.join("\n")}"
              puts "#{e.message}\n#{e.backtrace.join("\n")}"
              path = go_import_path(model_name: klass, id: @object.id)
              msg = { error: "Ошибка разбора файла: #{e.message}" }
              redirect_to path, flash: msg
            end
          end
        end
      end
    end
  end
end
