# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'pg', '>= 0.18', '< 2.0'
gem 'rails', '5.2.0'
gem 'turbolinks' # required for redirects even if using via webpack

gem 'sass'

gem 'axlsx'
gem 'roo'

gem 'rocket_cms_activerecord'

gem 'friendly_id', github: 'norman/friendly_id'
gem 'rails_admin', github: 'sferik/rails_admin'

gem 'pug-rails'

gem 'haml'
gem 'slim'

gem 'rs-webpack-rails', '~> 0.11.1'
gem 'sass-rails'

gem 'cancancan'
gem 'devise'
gem 'devise-i18n'

gem 'paperclip'

gem 'cloner'
gem 'puma'

gem 'sentry-raven'

gem 'uglifier'

gem 'active_model_serializers'
gem 'breadcrumbs_on_rails'
gem 'enumerize'
gem 'request_store'
gem 'rs_russian'

# windows
gem 'tzinfo-data' if Gem.win_platform?
gem 'wdm', '>= 0.1.0' if Gem.win_platform?

gem 'bootsnap', require: false

group :development do
  # gem 'binding_of_caller'
  # gem 'better_errors', github: 'charliesome/better_errors'
  # gem 'pry-rails'
  gem 'annotate'
  gem 'listen'

  gem 'capistrano', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano3-puma', require: false
end

group :test do
  gem 'email_spec'
  gem 'rspec-rails'

  gem 'factory_bot_rails'
  gem 'ffaker'

  gem 'capybara'
  gem 'capybara-webkit'
  gem 'childprocess'
  gem 'database_cleaner'
end
