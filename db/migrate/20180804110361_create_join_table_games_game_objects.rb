class CreateJoinTableGamesGameObjects < ActiveRecord::Migration[5.2]
  def change
    create_join_table :game_objects, :games do |t|
      t.index %i[game_id game_object_id], name: :g_go_idx
    end
  end
end
