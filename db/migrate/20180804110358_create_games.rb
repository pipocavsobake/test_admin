class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :title
      t.text :content
      t.jsonb :question_field_data
      t.references :user

      t.timestamps
    end
  end
end
