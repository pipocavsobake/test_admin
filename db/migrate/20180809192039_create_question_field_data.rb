# frozen_string_literal: true

class CreateQuestionFieldData < ActiveRecord::Migration[5.2]
  def change
    remove_column :games, :question_field_data, :jsonb
    create_table :question_field_data do |t|
      t.string :name
      t.string :kind
      t.string :answer_field
      t.references :game, foreign_key: true

      t.timestamps
    end
  end
end
