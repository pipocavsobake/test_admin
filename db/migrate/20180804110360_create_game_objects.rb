class CreateGameObjects < ActiveRecord::Migration[5.2]
  def change
    create_table :game_objects do |t|
      t.jsonb :data

      t.timestamps
    end
  end
end
