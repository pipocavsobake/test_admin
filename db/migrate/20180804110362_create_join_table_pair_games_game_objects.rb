class CreateJoinTablePairGamesGameObjects < ActiveRecord::Migration[5.2]
  def change
    create_join_table :game_objects, :pair_games do |t|
      # t.index [:game_object_id, :pair_game_id]
      t.index %i[pair_game_id game_object_id], name: :pg_go_idx
    end
  end
end
