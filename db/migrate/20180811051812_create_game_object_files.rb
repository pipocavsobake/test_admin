class CreateGameObjectFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :game_object_files do |t|
      t.attachment :file
      t.references :game_object, foreign_key: true
      t.string :key

      t.timestamps
    end
  end
end
