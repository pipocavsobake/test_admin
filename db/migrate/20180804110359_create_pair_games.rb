class CreatePairGames < ActiveRecord::Migration[5.2]
  def change
    create_table :pair_games do |t|
      t.string :title
      t.text :content
      t.string :label_kind
      t.string :value_kind
      t.jsonb :labels
      t.references :user

      t.timestamps
    end
  end
end
