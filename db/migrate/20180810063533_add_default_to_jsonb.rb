# frozen_string_literal: true

class AddDefaultToJsonb < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        change_column :pair_games, :labels, :jsonb, default: {}
      end
    end
  end
end
