require 'cloner'

class Dl < Cloner::Base
  no_commands do
    def rails_path
      File.expand_path("../../../config/environment", __FILE__)
    end
    def ssh_host
      'tests.shirykalov.ru'
    end
    def ssh_user
      'tests'
    end
    def remote_dump_path
      '/data/tests/tmp_dump'
    end
    def remote_app_path
      '/data/tests/app/current'
    end
  end

  desc "download", "clone files and DB from production"
  def download
    load_env
    clone_db
    rsync_public("ckeditor_assets")
    rsync_public("system")
  end
end

