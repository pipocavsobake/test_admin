# frozen_string_literal: true

# == Schema Information
#
# Table name: games
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  content    :text
#  user_id    :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class GameSerializer < Short::GameSerializer
  has_many :question_field_data, key: :questionFieldData
  has_many :game_objects, key: :objects
end
