# frozen_string_literal: true

module Short
  class GameSerializer < ActiveModel::Serializer
    attributes :id, :title, :content
  end
end
