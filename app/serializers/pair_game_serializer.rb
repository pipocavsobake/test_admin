# frozen_string_literal: true

# == Schema Information
#
# Table name: pair_games
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  content    :text
#  label_kind :string
#  value_kind :string
#  labels     :jsonb
#  user_id    :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#


class PairGameSerializer < Short::GameSerializer
  attributes :labelType, :valueType, :labels
  has_many :game_objects, key: :objects
  def labelType
    object.label_kind
  end

  def valueType
    object.value_kind
  end
end
