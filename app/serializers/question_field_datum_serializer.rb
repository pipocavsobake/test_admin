# frozen_string_literal: true

# == Schema Information
#
# Table name: question_field_data
#
#  id           :bigint(8)        not null, primary key
#  name         :string
#  kind         :string
#  answer_field :string
#  game_id      :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#


class QuestionFieldDatumSerializer < ActiveModel::Serializer
  attributes :name, :type, :answerField

  def type
    object.kind
  end

  def answerField
    object.answer_field
  end
end
