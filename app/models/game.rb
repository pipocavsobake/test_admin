# frozen_string_literal: true

# == Schema Information
#
# Table name: games
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  content    :text
#  user_id    :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Game < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :game_objects
  has_many :question_field_data, dependent: :destroy
  accepts_nested_attributes_for :question_field_data, allow_destroy: true
  rails_admin do
    field :title
    field :content, :ckeditor
    field :question_field_data
  end
  def check_keys!(ext_keys)
    raise StandardError, 'wrong keys' if (keys & ext_keys.sort).size != ext_keys.size
  end

  def keys
    question_field_data.pluck(:name, :answer_field).flatten.uniq.sort
  end

  def need_archive?
    question_field_data.map(&:kind).include?('image')
  end

  before_save do
    self.user ||= User.current
    true
  end
end
