# frozen_string_literal: true

# == Schema Information
#
# Table name: question_field_data
#
#  id           :bigint(8)        not null, primary key
#  name         :string
#  kind         :string
#  answer_field :string
#  game_id      :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class QuestionFieldDatum < ApplicationRecord
  extend Enumerize
  enumerize :kind, in: %i[text image]
  belongs_to :game
  rails_admin do
    hide
    field :name
    field :kind
    field :answer_field
  end
end
