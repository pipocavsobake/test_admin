# frozen_string_literal: true

# == Schema Information
#
# Table name: pair_games
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  content    :text
#  label_kind :string
#  value_kind :string
#  labels     :jsonb
#  user_id    :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# join words and laguages for example
class PairGame < ApplicationRecord
  extend Enumerize
  has_and_belongs_to_many :game_objects
  belongs_to :user, required: false
  enumerize :label_kind, in: %i[text]
  enumerize :value_kind, in: %i[text]
  rails_admin do
    field :title
    field :content, :ckeditor
    field :label_kind
    field :value_kind
    field :labels, :jsonb do
      partial 'form_jsonb'
    end
  end

  def check_keys!(ext_keys)
    raise StandardError, 'wrong keys' if (keys & ext_keys.sort).size != ext_keys.size
  end

  def keys
    labels.keys.sort
  end

  def need_archive?
    false
  end
  before_save do
    self.user ||= User.current
    true
  end
end
