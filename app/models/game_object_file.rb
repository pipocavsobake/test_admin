# frozen_string_literal: true

# == Schema Information
#
# Table name: game_object_files
#
#  id                :bigint(8)        not null, primary key
#  file_file_name    :string
#  file_content_type :string
#  file_file_size    :bigint(8)
#  file_updated_at   :datetime
#  game_object_id    :bigint(8)
#  key               :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class GameObjectFile < ApplicationRecord
  belongs_to :game_object
  has_attached_file :file
  do_not_validate_attachment_file_type :file
end
