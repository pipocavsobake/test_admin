# frozen_string_literal: true

# == Schema Information
#
# Table name: game_objects
#
#  id         :bigint(8)        not null, primary key
#  data       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class GameObject < ApplicationRecord
  has_and_belongs_to_many :games
  has_and_belongs_to_many :pair_games
  def as_json(_ops = {})
    data
  end
  rails_admin do
    hide
  end
end
