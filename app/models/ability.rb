# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    unless user.nil?
      if user.admin?
        can :read, :all
        can :manage, :all
      else
        game_ids = user.game_ids
        can :manage, [Game, PairGame], user: user
        can :manage, GameObject
        cannot :destroy, GameObject
        can :manage, QuestionFieldDatum, game_id: game_ids
      end
      cannot :destroy, Menu
      cannot :update, Menu
      admin_ui
    end
  end

  def admin_ui
    can :access, :rails_admin # grant access to rails_admin
    can :dashboard, :all # grant access to the dashboard
  end
end
