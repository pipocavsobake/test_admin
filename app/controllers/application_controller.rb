# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include RocketCMS::Controller
  before_action do
    RequestStore.store[:current_user] = current_user
  end
  before_action do
    add_breadcrumb 'Главная', '/'
  end

  def page_title # default page title
    'tests'
  end
end
