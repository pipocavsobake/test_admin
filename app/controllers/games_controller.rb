# frozen_string_literal: true

class GamesController < ApplicationController
  before_action do
    add_breadcrumb 'Тесты', games_path
  end
  def index
    @games = Game.page(params[:page])
    respond_to do |format|
      format.html
      format.json do
        render json: @games, each_serializer: Short::GameSerializer
      end
    end
  end

  def show
    @game = Game.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @game }
    end
  end
end
