# frozen_string_literal: true

class PairGamesController < ApplicationController
  before_action do
    add_breadcrumb 'Парные Тесты', pair_games_path
  end
  def index
    @games = PairGame.page(params[:page])
    respond_to do |format|
      format.html
      format.json do
        render json: @games, each_serializer: Short::GameSerializer
      end
    end
  end

  def show
    @game = PairGame.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @game }
    end
  end
end
