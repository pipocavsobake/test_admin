//= require ./fix.js
//= require pug-runtime
//= require templates/key_value.jst.pug

const checkBtn = () => {
  const $input = $(".js-jsonb-input");
  const $btn = $(".js-jsonb-add-btn");
  const $new = $(".js-jsonb-new-key");
  const keys = Object.keys(JSON.parse($input.val()));
  if ($new.val().length === 0 || keys.includes($new.val()))
    $btn.addClass("disabled");
  else $btn.removeClass("disabled");
};

const init = () => {
  const $input = $(".js-jsonb-input");
  if ($input.length === 0) return;
  const $pairs = $(".js-jsonb-pairs");
  const $new = $(".js-jsonb-new-key");
  const $btn = $(".js-jsonb-add-btn");
  $btn.off("click.jsonb").on("click.jsonb", () => {
    $btn.addClass("disabled");
    $pairs.append(
      JST["templates/key_value"]({
        label: $new.val(),
        klass: `jsonb-val jsonb-val-${$new.val()}`,
        val: ""
      })
    );
  });
  $new.off("input.jsonb").on("input.jsonb", checkBtn);
  const obj = JSON.parse($input.val());
  for (let key in obj) {
    $pairs.append(
      JST["templates/key_value"]({
        label: key,
        klass: `jsonb-val jsonb-val-${key}`,
        val: obj[key]
      })
    );
  }
};

$(document).on("input", ".jsonb-val", event => {
  const $input = $(".js-jsonb-input");
  const $t = $(event.target);
  $input.val(
    JSON.stringify({
      ...JSON.parse($input.val()),
      [$t.data("key")]: $t.val()
    })
  );
});
$(document).on("click", ".jsonb-remove", event => {
  const $input = $(".js-jsonb-input");
  const $t = $(event.target).closest(".jsonb-remove");
  const { [$t.data("key")]: omit, ...obj } = JSON.parse($input.val());
  $input.val(JSON.stringify(obj));
  $t.closest(".jsonb-pair").remove();
  checkBtn();
});
$(document).on("pjax:complete", init);
$(init);
