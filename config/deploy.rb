# frozen_string_literal: true

set :user, 'test_admin'
set :application, 'test_admin'
set :repo_url, 'https://gitlab.com/pipocavsobake/test_admin'

set :branch, ENV['REVISION'] || 'master'

set :deploy_to, '/home/test_admin/app'

# set :rvm_type, :user
# set :rvm_ruby_version, "2.5.0@#{fetch :application}"
set :use_sudo, false

set :puma_state, -> { File.join(shared_path, 'tmp', 'puma', 'state') }
set :puma_pid, -> { File.join(shared_path, 'tmp', 'puma', 'pid') }
set :puma_conf, -> { File.join(current_path, 'config', 'puma.rb') }

Rake::Task['puma:check'].clear
Rake::Task['puma:config'].clear
namespace :puma do
  task :check do
    puts 'override'
  end
  task :config do
  end
end

set :keep_releases, 20

set :linked_files, %w[config/database.yml config/secrets.yml]

set :linked_dirs, %w[log tmp vendor/bundle public/system public/uploads public/ckeditor_assets public/sitemap]
