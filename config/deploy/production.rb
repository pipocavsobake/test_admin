# frozen_string_literal: true

set :stage, :production

server 'tests.shirykalov.ru', user: 'test_admin', roles: %w[web app db]

set :rails_env, 'production'
