# == Schema Information
#
# Table name: game_objects
#
#  id         :bigint(8)        not null, primary key
#  data       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :game_object do
    data ""
  end
end
