# == Schema Information
#
# Table name: pair_games
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  content    :text
#  label_kind :string
#  value_kind :string
#  labels     :jsonb
#  user_id    :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :pair_game do
    title "MyString"
    content "MyText"
    label_kind "MyString"
    value_kind "MyString"
    labels ""
  end
end
