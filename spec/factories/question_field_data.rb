# == Schema Information
#
# Table name: question_field_data
#
#  id           :bigint(8)        not null, primary key
#  name         :string
#  kind         :string
#  answer_field :string
#  game_id      :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryBot.define do
  factory :question_field_datum do
    name "MyString"
    type ""
    answer_field "MyString"
  end
end
