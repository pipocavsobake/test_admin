# == Schema Information
#
# Table name: games
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  content    :text
#  user_id    :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :game do
    title "MyString"
    content "MyText"
    question_field_data ""
  end
end
