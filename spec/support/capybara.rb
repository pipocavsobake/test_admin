require 'capybara/rspec'
require 'capybara/webkit'

# https://github.com/teamcapybara/capybara/blob/master/lib/capybara.rb#L35
Capybara.configure do |config|
  config.default_driver = :webkit
  config.javascript_driver = :webkit

  # use the custom server
  config.server = :puma
  config.run_server = true
end
