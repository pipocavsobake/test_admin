import React from "react";
import ReactDOM from "react-dom";
import Test from "react-object-questions";
import axios from "axios";

const init = () => {
  const root = document.getElementById("game-root");
  if (!root) return;
  axios.get(window.location.pathname + ".json").then(({ data }) => {
    ReactDOM.render(<Test test={{ data }} startTest={() => {}} />, root);
  });
};

document.addEventListener("turbolinks:load", init);
