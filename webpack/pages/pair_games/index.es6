import React from "react";
import ReactDOM from "react-dom";
import { PairTest } from "react-object-questions";
import axios from "axios";

const init = () => {
  const root = document.getElementById("pair-game-root");
  if (!root) return;
  axios.get(window.location.pathname + ".json").then(data => {
    ReactDOM.render(<PairTest test={data.data} />, root);
  });
};

document.addEventListener("turbolinks:load", init);
